class ResponsiveMan {
  constructor(options = {}) {
    this.element = null;
    this.current = null;

    this.options = Object.assign({}, {
      elementId: 'responsive-man',
      breakpoints: [{
        label: 'xs',
      }, {
        label: 'sm',
        min: 768,
      }, {
        label: 'md',
        min: 992,
      }, {
        label: 'lg',
        min: 1200,
      }, ],
      broadcast: true
    }, options)

    this.options.breakpoints.sort((a, b) => {
      const aMin = a.min || 0;
      const bMin = b.min || 0;
      return aMin === bMin ? 0 : (aMin > bMin ? 1 : -1);
    });

    this.handleLoaded = this.handleLoaded.bind(this);
    this.handleResize = this.handleResize.bind(this);

    if (/comp|inter|loaded/.test(document.readyState)) {
      this.handleLoaded();
    } else {
      document.addEventListener('DOMContentLoaded', this.handleLoaded);
    }
    return {
      getSize: this.getSize.bind(this),
      is: this.is.bind(this),
    }
  }

  getSize() {
    if (this.element) {
      return window.getComputedStyle(this.element).getPropertyValue("content").replace(/\W/g, '');
    }
    return this.getSizeFrominnerWidth();
  }

  getSizeFrominnerWidth() {
    const {
      innerWidth
    } = window;
    const {
      breakpoints
    } = this.options;

    for (var i = breakpoints.length - 1; i >= 0; i--) {
      if ((breakpoints[i].min || 0) <= innerWidth) {
        return breakpoints[i].label;
      }
    }
    throw 'Could not find current breakpoint from innerWidth.  Did you set a default?';
  }

  is(check) {
    if (!Array.isArray(check)) {
      check = [check];
    }
    const size = this.getSize();

    for (var i = check.length - 1; i >= 0; i--) {
      if (check[i] === size) {
        return true;
      }
    }

    return false;
  }

  handleLoaded() {
    const {
      broadcast,
    } = this.options;

    this.makeStylesheet();
    this.makeElement();

    if (broadcast) {
      this.current = this.getSize();
      window.addEventListener('resize', this.handleResize)
    }
  }

  handleResize() {
    const size = this.getSize();
    if (size !== this.current) {
      const event = new CustomEvent('responsivemanchange', {
        bubbles: true,
        detail: {
          from: this.current,
          to: size
        }
      });
      this.current = size;
      this.element.dispatchEvent(event);
    }
  }

  makeElement() {
    const {
      elementId,
    } = this.options;

    const element = document.createElement('div');
    element.id = elementId;
    document.body.appendChild(element);
    this.element = element;
  }

  makeStylesheet() {
    const {
      head,
    } = document;
    const {
      elementId,
      breakpoints,
    } = this.options;
    const style = document.createElement('style');

    style.innerHTML = `#${elementId} { display: none; }`;

    const styles = breakpoints.map((breakpoint) => {
      let str = `#${elementId} { content: '${breakpoint.label}'; }`;
      if (breakpoint.min) {
        str = `@media(min-width: ${breakpoint.min}px) { ${str} }`;
      }
      return str;
    });

    style.innerHTML += styles.join("\n");

    head.appendChild(style);
  }
}

export default ResponsiveMan;
