# Responsive Man!
A pure javascript responsive manager utility package.  Uses CSS `content` property to determine browser's computed breakpoint, avoiding discrepancies between JS window and CSS viewport size reporting

## Usage

    import ResponsiveMan from 'responsive-man'
    const rm = new ResponsiveMan();
    
    console.log(rm.getSize())
    // returns breakpoint label
    
    console.log(rm.is('xs'))
    // Are we at XS breakpoint?

    console.log(rm.is(['sm', 'md']))
    // Are we at the SM or MD breakpoint?
    
    console.log(rm.is('md'))
    // Are we at md breakpoint?

    window.addEventListener('responsivemanchange', (e) => {
        const { from, to } = e.detail;
        console.log(`We moved from "${from}" to "${to}"!`);
        // We moved from "xs" to "sm"!
    });
    

## Options

 __elementId__ _string_ The ID of the element that will be created to monitor current breakpoint.
 _default_: `responsive-man`

__broadcast__ _boolean_ Whether or not to watch the window's `resize` event and broadcast the `responsivemanchange` event when the breakpoint changes.
_default_: `false`

 __breakpoints__ _array_ Array of breakpoints to be used, each with a `label` and a `min` pixel size.
_default_:

    [{
        label: 'xs',
      }, {
        label: 'sm',
        min: 768,
      }, {
        label: 'md',
        min: 992,
      }, {
        label: 'lg',
        min: 1200,
    }]
    
## Methods
__getSize( )__ _string_  returns the current breakpoint's label

__is__ __(__ _string_ label __)__ _boolean_ returns true if current breakpoint is _label_

## Events
__responsivemanchange__ Fires when the window is resized _and_ the breakpoint has changed.  `event.detail` carries an object containing `from` and `to` breakpoint labels.

_NB: If using jQuery you may need to look at event.originalEvent.detail_

## Notes
- This package is designed to be used with webpack and babel.  The `main` entry in package json points to the ES6 module in `/src`.  If this causes problems with your build process you can find the transpiled and bundled code in the `/dist` folder.